
function love.load()
	love.filesystem.load("enemies.lua")()

	math.randomseed(love.timer.getTime())
	fontbig = love.graphics.newFont(36)
	love.graphics.setFont(fontbig)
	fontsmall = love.graphics.newFont(16)
	img = love.graphics.newImage("img/elephant_bg.png")

	gamemode = "menu"

	-- initGame()
	
end

function initGame()
	gamemode = "game"
	initEnemies()

	board = {}
	for i=1,160 do
		board[i] = {}
		for j=1,120 do
			board[i][j] = 0
		end
	end

	local bw,bh = table.getn(board),table.getn(board[1])
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])


	for i=1,bw do
		board[i][1] = 1
		-- board[i][2] = 1
		board[i][bh] = 1
		-- board[i][bh-1] = 1
	end
	for j=1,bh do
		board[1][j] = 1
		-- board[2][j] = 1
		board[bw][j] = 1
		-- board[bw-1][j] = 1
	end

	circleRadiusRespawn = 170

	player1 = {
		ipos = {x = 1, y = 1},
		old_ipos = {x = 1, y = 1},
		pos = {x = sw/2, y = sh/2},
		startPos = {x = sw/2, y = sh/2},
		dir = {x = 0, y = 0},
		size = {x = 1, y = 1},
		speed = 100,
		path = {},
		id = 1,
		score = 0,
		keys = { left = "a", right = "d", up = "w", down = "s"},
		circleRadius = circleRadiusRespawn,
		respawnTimer = 0,
		blockedKeys = {false,false},
		pathColor = {0,128,255},
		playerColor = {0,255,255},
	}

	player2 = {
		ipos = {x = 100, y = 100},
		old_ipos = {x = 100, y = 100},
		pos = {x = love.graphics.getWidth()-sw/2, y = love.graphics.getHeight()-sh/2},
		startPos = {x = love.graphics.getWidth()-sw/2, y = love.graphics.getHeight()-sh/2},
		dir = {x = 0, y = 0},
		size = {x = 1, y = 1},
		speed = 100,
		path = {},
		id = 2,
		score = 0,
		keys = { left = "left", right = "right", up = "up", down = "down"},
		circleRadius = circleRadiusRespawn,
		respawnTimer = 0,
		blockedKeys = {false,false},
		pathColor = {128,0,255},
		playerColor = {255,0,255},
	}

	Players = {player1,player2}

	canvas = love.graphics.newCanvas(800,600)
	love.graphics.setCanvas(canvas)
    canvas:clear()
    drawboard()
    love.graphics.setCanvas()
    
    currentThres = 0.15
    blocksCovered = 0
    blocksToFill = (table.getn(board)-2)*(table.getn(board[1])-2)

    startEnemies()
end


function updateMenu(dt)
end

function love.update(dt)
	if gamemode == "menu" then
		updateMenu(dt)
	elseif gamemode == "game" then
		updatePlayer(dt)
		updateEnemies(dt)
		checkVictory(dt)
	elseif gamemode == "end" then
	-- nothing?
		endtimer = endtimer - dt
		if endtimer <= 0 then gamemode = "menu" end
	end
end

function checkVictory(dt)
	if blocksCovered/blocksToFill > 0.8 then
		gamemode = "end"
		endtimer = 12
	end
end

function updatePlayer(dt)
	updateSinglePlayer(dt,player1)
	updateSinglePlayer(dt,player2)

	--  frontal collision
	if player1.ipos.x == player2.ipos.x and player1.ipos.y == player2.ipos.y then
		killPlayer(player1)
		killPlayer(player2)
	end

	-- now check the moves
	playerMoved(player1)
	playerMoved(player2)
end

function manageKeys(player)
	if not player.blockedKeys[1] and math.abs(player.dir.x) ~= 1 then
		if love.keyboard.isDown(player.keys.left) then
			player.dir = { x = -1, y = 0 }
		elseif love.keyboard.isDown(player.keys.right) then
			player.dir = { x = 1, y = 0 }
		end
			-- stop mid way
	if love.keyboard.isDown(player.keys.up) and
		player.dir.y>0 and 
		board[player.ipos.x][player.ipos.y] and 
		board[player.ipos.x][player.ipos.y]%5 == 1 then
			player.dir.y = 0
			player.blockedKeys[2] = true
	end
	if love.keyboard.isDown(player.keys.down) and
		player.dir.y<0 and 
		board[player.ipos.x][player.ipos.y] and 
		board[player.ipos.x][player.ipos.y]%5 == 1 then
			player.dir.y = 0
			player.blockedKeys[2] = true
	end

	end
	if not player.blockedKeys[2] and math.abs(player.dir.y) ~= 1 then
		if love.keyboard.isDown(player.keys.up) then
			player.dir = { x = 0, y = -1 }
		elseif love.keyboard.isDown(player.keys.down) then
			player.dir = { x = 0, y = 1 }
		end
		-- stop mid way
		if love.keyboard.isDown(player.keys.right) and
			player.dir.x<0 and 
			board[player.ipos.x][player.ipos.y] and 
			board[player.ipos.x][player.ipos.y]%5 == 1 then
				player.dir.x = 0
				player.blockedKeys[1] = true

		end
		if love.keyboard.isDown(player.keys.left) and
			player.dir.x>0 and 
			board[player.ipos.x][player.ipos.y] and 
			board[player.ipos.x][player.ipos.y]%5 == 1 then
				player.dir.x = 0
				player.blockedKeys[1] = true
		end
	end

	if player.blockedKeys[1] and not love.keyboard.isDown(player.keys.left) and not love.keyboard.isDown(player.keys.right) then
		player.blockedKeys[1] = false
	end

	if player.blockedKeys[2] and not love.keyboard.isDown(player.keys.up) and not love.keyboard.isDown(player.keys.down) then
		player.blockedKeys[2] = false
	end
end

function updateSinglePlayer(dt, player)
	if player.respawnTimer > 0 then
		player.respawnTimer = player.respawnTimer - dt
		return
	end

	manageKeys(player)

	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])
	player.size = {x = sw, y = sh}

	local oldpos = { x = player.pos.x, y = player.pos.y }

	player.pos.x = player.pos.x + player.speed * player.dir.x * dt
	player.pos.y = player.pos.y + player.speed * player.dir.y * dt

	player.pos.x = math.max(sw/2, math.min(player.pos.x, love.graphics.getWidth() - player.size.x/2))
	player.pos.y = math.max(sh/2, math.min(player.pos.y, love.graphics.getHeight() - player.size.y/2))

	player.old_ipos = {x = player.ipos.x, y = player.ipos.y}

	player.ipos.x = math.floor(player.pos.x / sw - 0.5)+1
	player.ipos.y = math.floor(player.pos.y / sh - 0.5)+1

	if oldpos.x == player.pos.x then player.dir.x = 0 end
	if oldpos.y == player.pos.y then player.dir.y = 0 end

	if player.dir.x == 0 then
		player.pos.x = (player.ipos.x-1) * sw + sw/2
	end
	if player.dir.y == 0 then
		player.pos.y = (player.ipos.y-1) * sh + sh/2
	end

	-- circle radius
	if player.circleRadius > 0 then
		player.circleRadius = player.circleRadius * math.pow(0.95, 60*dt)
	end

end

function playerMoved(player)
	if player.old_ipos.x == player.ipos.x and player.old_ipos.y == player.ipos.y then
		return
	end

	local oldCell = board[player.old_ipos.x][player.old_ipos.y] % 5
	local newCell = board[player.ipos.x][player.ipos.y] % 5

	if (oldCell == 2 or oldCell == 3) and newCell == 1 then
		player.dir = {x = 0, y = 0}
		oldCell = 2
		fillArea(player)
	end

	if newCell == 2 then
		killPlayer(player)
		oldCell = 0
	end

	if newCell == 0 then
		if player.dir.x < 0 or player.dir.y < 0 then
			board[player.ipos.x][player.ipos.y] = 3 + player.id * 5
		else
			board[player.ipos.x][player.ipos.y] = 2 + player.id * 5
		end
		table.insert(player.path, {x = player.ipos.x, y = player.ipos.y})
	end

	if oldCell == 3 then
		board[player.old_ipos.x][player.old_ipos.y] = 2 + player.id * 5
	end
end

function killPlayer(player)
	player.dir={x=0,y=0}
	player.pos = {x = player.startPos.x, y = player.startPos.y}

	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])

	player.ipos = {x = math.floor(player.pos.x/sw-0.5)+1, y = math.floor(player.pos.y/sh-0.5)+1}

	player.old_ipos = {x = player.ipos.x, y = player.ipos.y}
	cleanPlayerPathAndMap(player)
	player.circleRadius = circleRadiusRespawn
	player.respawnTimer = 2

end

function cleanPlayerPathAndMap(player)
	for i,v in ipairs(player.path) do
		board[v.x][v.y] = 0
	end
	cleanPlayerPath(player)
end

function cleanPlayerPath(player)
	player.path = {}
end

function fillArea(player)
	-- find two candidates
	if table.getn(player.path) == 0 then return end
	local dirs = { {-1,0},{1,0},{0,-1},{0,1} }
	local candidates = {}
	for j,w in ipairs(player.path) do
		for i,v in ipairs(dirs) do
			local cnd = {x = player.path[j].x + v[1], y = player.path[j].y+v[2]}
			if board[cnd.x][cnd.y] == 0 then
				table.insert(candidates,cnd)
			end
		end
	end
	
	-- prepare empty marks map
	local marks = {}
	for i,v in ipairs(board) do
		marks[i] = {}
		for j,w in ipairs(v) do
			marks[i][j] = 0
		end
	end

	local winners = {}

	-- eval candidates
	for icnd,cnd in ipairs(candidates) do
		local buffer = {}
		cnd.score = #player.path
		cnd.buf = {{x = cnd.x, y = cnd.y}}

		table.insert(buffer,{x = cnd.x, y = cnd.y})
		if marks[cnd.x][cnd.y] ~= 1 then
			marks[cnd.x][cnd.y] = 1
			cnd.score = cnd.score + 1

			while table.getn(buffer)>0 do
				local np = table.remove(buffer,1)
				for i,v in ipairs(dirs) do
					local newpoint = {x=np.x+v[1],y=np.y+v[2]}
					if board[newpoint.x][newpoint.y] == 0 and marks[newpoint.x][newpoint.y]== 0 then
						table.insert(buffer,newpoint)
						table.insert(cnd.buf,newpoint)
						marks[newpoint.x][newpoint.y] = 1
						cnd.score = cnd.score + 1
					end
				end
			end

			table.insert(winners, cnd)
		end
	end

	-- discard "full board"
	table.sort(winners, function(a,b) return a.score > b.score end)
	table.remove(winners,1)

	-- merge all others
	local fillbuf = {}
	for j,cnd in ipairs(winners) do
		for i,v in ipairs(cnd.buf) do
			table.insert(fillbuf, v)
		end
	end

	-- append path
	for i,v in ipairs(player.path) do
		table.insert(fillbuf, v)
	end



	love.graphics.setCanvas(canvas)
	if player.id == 1 then
		love.graphics.setColor(128,255,255)
	else
		love.graphics.setColor(255,128,255)
	end


	for i,v in ipairs(fillbuf) do
		board[v.x][v.y] = 1
		drawBgBlock(v.x,v.y)
	end

	love.graphics.setCanvas()

	-- kill enemies
	for j,w in ipairs(fillbuf) do
		local enemynum = table.getn(Enemies.list)
		for i=1,enemynum do
			local n = enemynum - i + 1
			local enemy = Enemies.list[n]
			if enemy.etype == 1 and enemy.ipos.x == w.x and enemy.ipos.y == w.y then
				-- table.remove(Enemies.list,n)
				respawnEnemy(enemy)
			end
		end
	end

	-- count new score
	coverBlocks( player, table.getn(fillbuf) )

	-- clean player path
	cleanPlayerPath(player)
end

function coverBlocks(player, num)
	player.score = player.score + num
	blocksCovered = blocksCovered + num 
	while blocksCovered / blocksToFill > currentThres do
		currentThres = currentThres + 0.13666
		addEnemyIn()
		addEnemyOut()
	end
end

function drawBgBlock(_x,_y)
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])
	local coord = {x = (_x-1)*sw, y = (_y-1)*sh}
		love.graphics.draw(img,
			love.graphics.newQuad(
				coord.x,coord.y,sw,sh,
				img:getWidth(),img:getHeight()),
			coord.x,coord.y)
	-- love.graphics.setColor(0,255,0)
	-- love.graphics.rectangle("fill",coord.x,coord.y,sw,sh)
end

function drawboard()
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])
	for i,v in ipairs(board) do
		for j,w in ipairs(v) do
			local dodraw = false
			if w == 1 then
				love.graphics.setColor(0,64,0)
				dodraw = true
			elseif w == 2 then
				love.graphics.setColor(255,255,0)
				dodraw = true
			end

			if dodraw then
				love.graphics.rectangle("fill",(i-1)*sw,(j-1)*sh,sw,sh)
			end
		end
	end
end

function drawScore()
	-- love.graphics.setColor(255,0,255)
	-- love.graphics.print(math.floor(blocksCovered/blocksToFill*100).." %",380,10)
	love.graphics.setFont(fontbig)
	love.graphics.setColor(0,255,255)
	love.graphics.print(math.floor(player1.score/blocksToFill*100).." %",100,10)

	love.graphics.setColor(255,0,255)
	love.graphics.print(math.floor(player2.score/blocksToFill*100).." %",650,10)

end

function drawPlayer(player)
	if player.respawnTimer > 0 then 
		love.graphics.setColor(255,255,255)
		-- love.graphics.setColor(unpack(player.playerColor))
		-- local x0,y0 = 10,10
		-- if player == Players[2] then
		-- 	x0,y0 = love.graphics.getWidth() - 140, love.graphics.getHeight()-40
		-- end
		local pn = 1
		local y0 = love.graphics.getHeight() * 0.5 - fontsmall:getHeight()
		if player == Players[2] then 
			pn = 2
			y0 = y0 + fontsmall:getHeight() 
		end
		local txt = "PLAYER "..pn.." RESPAWN IN "..math.floor(player.respawnTimer+1)
		local x0 = (love.graphics.getWidth()-fontsmall:getWidth(txt)) * 0.5
		love.graphics.setFont(fontsmall)
		love.graphics.print(txt,x0,y0)
		return 
	end

	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])
	love.graphics.setColor(unpack(player.pathColor))
	-- love.graphics.setColor(0,128,255)
	local ilimit = table.getn(player.path)
	for i,v in ipairs(player.path) do
		if i==ilimit and (player.dir.x<0 or player.dir.y<0) then break end
		love.graphics.rectangle("fill",(v.x-1)*sw,(v.y-1)*sh,sw,sh)
	end

	if player.circleRadius > 1 then
		local pc = player.playerColor
		love.graphics.setColor(pc[1],pc[2],pc[3],64)
		love.graphics.circle("fill",player.pos.x,player.pos.y,player.circleRadius,player.circleRadius)
	end
	love.graphics.setColor(unpack(player.playerColor))
	love.graphics.rectangle("fill",player.pos.x-player.size.x/2,player.pos.y-player.size.y/2,
		player.size.x,player.size.y)

end

function love.draw()
	if gamemode == "menu" then
		drawMenu()
	elseif gamemode == "game" then
		drawGame()
	elseif gamemode == "end" then 
		drawEnduf() 
	end
end

function drawMenu()
	love.graphics.setColor(255,255,255)
	love.graphics.setFont(fontbig)
	local txt = "ELEPHIX"
	local x0,y0 = (love.graphics.getWidth() - fontbig:getWidth(txt))*0.5, love.graphics.getHeight()*0.5 - 50
	love.graphics.print(txt,x0,y0)
	love.graphics.setFont(fontsmall)
	txt = "Controls: WASD / arrows"
	x0,y0 = (love.graphics.getWidth() - fontsmall:getWidth(txt))*0.5, love.graphics.getHeight()*0.5
	love.graphics.print(txt,x0,y0)
	txt = "press space to start"
	x0,y0 = (love.graphics.getWidth() - fontsmall:getWidth(txt))*0.5, love.graphics.getHeight()*0.5 + 20
	love.graphics.print(txt,x0,y0)
end

function drawGame()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(canvas)

	drawEnemies()

	drawPlayer(player1)
	drawPlayer(player2)

	drawScore()

end

function drawEnduf()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(img,0,0)

	if player1.score > player2.score then
		love.graphics.setColor(0,255,255)
		love.graphics.print("BLUE PLAYER WINS",200,100)
	else
		love.graphics.setColor(255,0,0)
		love.graphics.print("PURPLE PLAYER WINS",200,100)
	end
end

function love.keypressed(key)
	if key == "escape" then
		love.event.push("quit")
		return
	end

	if gamemode == "menu" and key == " " then
		initGame()
	end
end