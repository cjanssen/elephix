function initEnemies()
	Enemies = {}
end

function startEnemies()
	Enemies.list = {}

	for i=1,3 do
		addEnemyIn()
	end
	addEnemyOut()
	-- special: last enemy goes in 45 degree angles only
	local lastOut = Enemies.list[#Enemies.list]
	lastOut.dir.x = math.sqrt(2)/2 * (math.random(2)*2-3)
	lastOut.dir.y = math.sqrt(2)/2 * (math.random(2)*2-3)
end

function respawnEnemy(enemy)
	if enemy.etype == 1 then
		local _ix,_iy = 0,0
		local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])

		-- find valid coord
		local valid = false
		while not valid do
			_ix,_iy = math.random(table.getn(board)-2)+1,math.random(table.getn(board[1])-2)+1
			if board[_ix][_iy] == 0 then
				valid = true
			end
		end
		enemy.ipos = {x = _ix, y = _iy}
		enemy.pos = { x = (_ix-0.5)*sw, y = (_iy-0.5) * sh }
		enemy.invalidmoves = 0
	end
end

function addEnemyIn()
	local _ix,_iy = 0,0
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])

	-- find valid coord
	local valid = false
	while not valid do
		_ix,_iy = math.random(table.getn(board)-2)+1,math.random(table.getn(board[1])-2)+1
		if board[_ix][_iy] == 0 then
			valid = true
		end
	end

	local ang = math.random(math.pi*2)
	table.insert(Enemies.list, {
		pos = { x = (_ix-0.5)*sw, y = (_iy-0.5) * sh},
		ipos = { x = _ix, y = _iy}, 
		dir = { x = math.cos(ang), y = math.sin(ang) },
		etype = 1,
		speed = 100 + math.random(100),
		invalidmoves = 0
		})
end

function addEnemyOut()
	-- local _ix,_iy = math.random(table.getn(board)-2)+1,math.random(table.getn(board[1])-2)+1
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])
	local bw,bh = table.getn(board),table.getn(board[1])

	local validpositions = blocksCovered + table.getn(board)*2 + table.getn(board[1])*2
	local num = math.random(validpositions)
	local _ix,_iy = 1,1
	local j = -1
	while num > 0 do
		j = (j + 1)%(bw*bh)
		_ix = j%bw + 1
		_iy = math.floor(j/bw)+1
		if board[_ix][_iy] == 1 then
			num = num - 1
		end
	end

	local ang = math.random(math.pi*2)
	table.insert(Enemies.list, {
		pos = { x = (_ix-0.5)*sw, y = (_iy-0.5) * sh},
		ipos = { x = _ix, y = _iy}, 
		dir = { x = math.cos(ang), y = math.sin(ang) },
		etype = 2,
		speed = 100
		})
end

function updateEnemies(dt)
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])

	for i,enemy in ipairs(Enemies.list) do
		newpos = {
			x = enemy.pos.x + enemy.speed * enemy.dir.x * dt,
			y = enemy.pos.y + enemy.speed * enemy.dir.y * dt
		}

		if newpos.x < sw/2 then 
			newpos.x = sw/2
			enemy.dir.x = math.abs(enemy.dir.x)
		end
		if newpos.y < sh/2 then 
			newpos.y = sh/2
			enemy.dir.y = math.abs(enemy.dir.y)
		end
		if newpos.x > love.graphics.getWidth() - sw/2 then
			-- enemy.pos.x = love.graphics.getWidth() - sw/2
			-- enemy.dir.x = -enemy.dir.x
			newpos.x = love.graphics.getWidth() - sw/2
			enemy.dir.x = -math.abs(enemy.dir.x)
		end
		if enemy.pos.y > love.graphics.getHeight() - sh/2 then
			-- enemy.pos.y = love.graphics.getHeight() - sh/2
			-- enemy.dir.y = -enemy.dir.y
			newpos.y = love.graphics.getHeight() - sh/2 
			enemy.dir.y = -math.abs(enemy.dir.y)
		end

		local _ix,_iy = math.floor(newpos.x/sw)+1,math.floor(newpos.y/sh)+1

		_ix,_iy = correctIxIy(_ix,_iy)

		if _ix ~= enemy.ipos.x or _iy ~= enemy.ipos.y then
			-- check collision with player
			for j,player in ipairs(Players) do
				if _ix == player.ipos.x and _iy == player.ipos.y then
					killPlayer(player)
				end
			end

			local blockType = 0

			-- check collision with wall (inenemy)
			if enemy.etype == 1 then
				_ix,_iy = correctIxIy(_ix,_iy)
				-- enemy.pos = {x = oldpos.x, y = oldpos.y}
				if _ix and _iy and board[_ix][_iy] and _ix>0 and board[_ix][_iy] ~= 0 then
					blockType = board[_ix][_iy]
					enemy.invalidmoves = enemy.invalidmoves + 1

					local function horzBounce()
						newpos.x = enemy.pos.x
						_ix = enemy.ipos.x
						enemy.dir.x = -enemy.dir.x
					end
					local function vertBounce()
						newpos.y = enemy.pos.y
						_iy = enemy.ipos.y
						enemy.dir.y = -enemy.dir.y
					end

					if not board[enemy.ipos.x][_iy] or board[enemy.ipos.x][_iy] == 0 then
						horzBounce()
					elseif not board[_ix][enemy.ipos.y] or board[_ix][enemy.ipos.y] == 0 then
						vertBounce()
					else
						horzBounce()
						vertBounce()
					end
				
					_ix,_iy = correctIxIy(_ix,_iy)

				else
					enemy.invalidmoves = 0
				end
			end

			-- check collision with wall (outenemy)
			if enemy.etype == 2 then
				_ix,_iy = correctIxIy(_ix,_iy)
				if _ix and _iy and board[_ix][_iy] and board[_ix][_iy] ~= 1 then
					blockType = board[_ix][_iy]
					local function horzBounce()
						newpos.x = enemy.pos.x
						_ix = enemy.ipos.x
						enemy.dir.x = -enemy.dir.x
					end
					local function vertBounce()
						newpos.y = enemy.pos.y
						_iy = enemy.ipos.y
						enemy.dir.y = -enemy.dir.y
					end

					if not board[enemy.ipos.x][_iy] or board[enemy.ipos.x][_iy] == 1 then
						horzBounce()
					elseif not board[_ix][enemy.ipos.y] or board[_ix][enemy.ipos.y] == 1 then
						vertBounce()
					else
						horzBounce()
						vertBounce()
					end
					_ix,_iy = correctIxIy(_ix,_iy)
				end
			end

			-- kill owner
			if blockType % 5 == 2 or blockType % 5 == 3 then
				local playerid = math.floor(blockType / 5)

				if playerid >= 1 then
					killPlayer(Players[playerid])
				end
			end

			_ix,_iy = correctIxIy(_ix,_iy)
			enemy.ipos.x = _ix
			enemy.ipos.y = _iy	
				
		end

		enemy.pos.x = newpos.x
		enemy.pos.y = newpos.y

		if enemy.invalidmoves and enemy.invalidmoves>10 then
			respawnEnemy(enemy)
		end
	end
end

function correctIxIy(_ix,_iy)
	if _ix<=0 then _ix = 1 end
	if _iy<=0 then _iy = 1 end
	if _ix>table.getn(board) then _ix = table.getn(board) end
	if _iy>table.getn(board[1]) then _iy = table.getn(board[1]) end
	return _ix,_iy
end

function drawEnemies()
	local sw,sh = love.graphics.getWidth()/table.getn(board), love.graphics.getHeight()/table.getn(board[1])

	for i,enemy in ipairs(Enemies.list) do
		if enemy.etype == 1 then
			love.graphics.setColor(255,0,0)
		elseif enemy.etype == 2 then
			love.graphics.setColor(255,192,32)
		end
		love.graphics.rectangle("fill",enemy.pos.x-sw/2,enemy.pos.y-sh/2,sw,sh)
	end
end